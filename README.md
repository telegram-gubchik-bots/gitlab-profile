# Telegram Bots Group

Welcome to the Telegram Bots GitLab group! This space is dedicated to various Telegram bots. Below are the repositories associated with this group:

## 1. [WeatherGubchikBot](link-to-weathergubchikbot-repo)
**Description:**
The `WeatherGubchikBot` is your go-to bot for quickly retrieving weather information in cities around the world. Stay updated on current weather conditions effortlessly.

## 2. [SmileGubchikBot](link-to-smilegubchikbot-repo)
**Description:**
The `SmileGubchikBot` brings joy to your chats with a collection of jokes and memes. Lighten the mood and share a laugh with friends using this entertaining bot.

<!--
## 3. [PostGubchikBot](link-to-postgubchikbot-repo)
**Description:**
The `PostGubchikBot` serves as an assistant for channel and chat owners. It automates the process of publishing posts at specified intervals and settings, making content management more efficient.

## 4. [MagicHeartGubchikBot](link-to-magicheartgubchikbot-repo)
**Description:**
The `MagicHeartGubchikBot` is a Telegram bot that adds a touch of magic to your conversations. Enjoy an animated magic heart directly in your dialog for a visually appealing experience.

> This README.md generated with assistance from ChatGPT